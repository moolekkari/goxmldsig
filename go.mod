module gitlab.com/moolekkari/goxmldsig

go 1.13

require (
	github.com/beevik/etree v1.2.0
	github.com/jonboulle/clockwork v0.3.0
	github.com/moolekkari/goxmldsig v0.0.0-20230928063006-c6d5b66ea258
	github.com/russellhaering/goxmldsig v1.2.0
	github.com/stretchr/testify v1.8.0
)
